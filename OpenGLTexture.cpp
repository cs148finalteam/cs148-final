
#include "OpenGLTexture.h"

std::unique_ptr<OpenGLTexture2D> OpenGLTexture2D::Create(int width, int height, const void* data)
{
    return std::make_unique<OpenGLTexture2D>(CreateOnlyThroughFactory(), width, height, data);
}

OpenGLTexture2D::OpenGLTexture2D(const CreateOnlyThroughFactory&, int width, int height, const void* data)
    : OpenGLObject()
    , m_id()
    , m_width(width)
    , m_height(height)
{
    START_GL_OPERATIONS();
    
    GL_OP(glActiveTexture(GL_TEXTURE0));
    GL_OP(glGenTextures(1, &m_id));
    GL_OP(glBindTexture(GL_TEXTURE_2D, m_id));
    GL_OP(glTexStorage2D(GL_TEXTURE_2D, 1, GL_RGB8, width, height));
    GL_OP(glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, width, height, GL_RGB, GL_FLOAT, data));
	
    SetWrapMode(WrapMode::Clamp);
}

OpenGLTexture2D::~OpenGLTexture2D()
{
    START_GL_OPERATIONS();

    GL_OP(glDeleteTextures(1, &m_id));
}

GLenum OpenGLTexture2D::GetObjectType() const
{
    return GL_TEXTURE;
}

GLuint OpenGLTexture2D::GetId() const
{
    return m_id;
}

void OpenGLTexture2D::Bind() const
{
    START_GL_OPERATIONS();

    GL_OP(glBindTexture(GL_TEXTURE_2D, m_id));
}

void OpenGLTexture2D::SetWrapMode(WrapMode wrapMode)
{
    START_GL_OPERATIONS();

    Bind();

    switch (wrapMode) {
    case WrapMode::Clamp:
        GL_OP(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
	    GL_OP(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));
        break;
    case WrapMode::Repeat:
	    GL_OP(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT));
	    GL_OP(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT));
        break;
    default:
        break;
    }
}

int OpenGLTexture2D::GetWidth() const
{
    return m_width;
}

int OpenGLTexture2D::GetHeight() const
{
    return m_height;
}
