
#pragma once

#include "OpenGLObject.h"

//---------------------------------------------------
// OpenGLProgram
//
// Encapsulates an OpenGL shader object.
//---------------------------------------------------
class OpenGLShader : public OpenGLObject
{
public:
    OpenGLShader(GLenum type, const char* pSource);
    ~OpenGLShader();
    
    virtual GLenum GetObjectType() const override;
    virtual GLuint GetId() const override;

private:
    GLenum m_type;
    GLuint m_id;
};
