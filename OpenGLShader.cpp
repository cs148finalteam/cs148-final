
#include "OpenGLShader.h"

OpenGLShader::OpenGLShader(GLenum type, const char* pSource)
    : OpenGLObject()
    , m_type(type)
    , m_id()
{
    START_GL_OPERATIONS();

    m_id = glCreateShader(type);
    if (!m_id) {
        throw std::runtime_error("Failed shader creation");
    }

    GL_OP(glShaderSource(m_id, 1, &pSource, nullptr));
    GL_OP(glCompileShader(m_id));

    GLint compiled = GL_FALSE;
    GL_OP(glGetShaderiv(m_id, GL_COMPILE_STATUS, &compiled));
    if (compiled != GL_TRUE)
    {
        GLsizei log_length = 0;
        GLchar message[1024];
        GL_OP(glGetShaderInfoLog(m_id, 1024, &log_length, message));
        BREAK_IF_DEBUGGING();
        throw std::runtime_error(message);
    }
}

OpenGLShader::~OpenGLShader()
{
    START_GL_OPERATIONS();

    if (m_id) {
        GL_OP(glDeleteShader(m_id));
    }
}

GLenum OpenGLShader::GetObjectType() const
{
    return GL_SHADER;
}

GLuint OpenGLShader::GetId() const
{
    return m_id;
}

