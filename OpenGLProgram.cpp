
#include "OpenGLProgram.h"

#include <memory>
#include <string>
#include <fstream>
#include <streambuf>

#include <GL/glew.h>
#include <GL/gl.h>

#include "OpenGLUtil.h"

OpenGLProgram::OpenGLProgram()
    : OpenGLObject()
    , m_id()
    , m_shaders()
    , m_isLinked(GL_FALSE)
{
}

OpenGLProgram::~OpenGLProgram()
{
    START_GL_OPERATIONS();

    GL_OP(glDeleteProgram(m_id));
}

void OpenGLProgram::BeginConstruction()
{
    m_id = glCreateProgram();

    GLuint shader = glCreateShader(GL_VERTEX_SHADER);
    GL_OP(glDeleteShader(shader));
}

void OpenGLProgram::AddShader(GLenum type, const char* pSource)
{
    m_shaders.push_back(std::make_unique<OpenGLShader>(type, pSource));
}

void OpenGLProgram::AddShaderFromFile(GLenum type, const std::string& file)
{
    std::ifstream in(file.c_str());
    if (!in) {
        throw std::logic_error(std::string("Can't read ") + file);
    }
    std::string str = std::string(std::istreambuf_iterator<char>(in), std::istreambuf_iterator<char>());

    AddShader(type, str.c_str());
}

bool OpenGLProgram::EndConstruction()
{
    START_GL_OPERATIONS();

    for (const auto& shader : m_shaders) {
        GL_OP(glAttachShader(m_id, shader->GetId()));
    }

    GL_OP(glLinkProgram(m_id));
    GL_OP(glGetProgramiv(m_id, GL_LINK_STATUS, &m_isLinked));

    for (const auto& shader : m_shaders) {
        GL_OP(glDetachShader(m_id, shader->GetId()));
    }
    m_shaders.clear();

    if (!m_isLinked) {
        GLint length = 0;
        GL_OP(glGetProgramiv(m_id, GL_INFO_LOG_LENGTH, &length));

        char buf[256];
        GL_OP(glGetProgramInfoLog(m_id, sizeof(buf), &length, buf));
        BREAK_IF_DEBUGGING();
    }

    return true;
}

void OpenGLProgram::UseProgram() const
{
    START_GL_OPERATIONS();

    GL_OP(glUseProgram(m_id));
}

GLenum OpenGLProgram::GetObjectType() const
{
    return GL_PROGRAM;
}

GLuint OpenGLProgram::GetId() const
{
    return m_id;
}
