#ifndef _CRT_SECURE_NO_WARNINGS
#define _CRT_SECURE_NO_WARNINGS
#endif

#include "TerrainMesh.h"
#include <algorithm>
#include <random>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <Windows.h>
#include "SimpleImage.h"


/*
Uses Diamond-Square algorithm to produce terrain mesh.

Credit: Diamond Square Algorithm
http://www.gameprogrammer.com/fractal.html#diamond
http://srchea.com/terrain-generation-the-diamond-square-algorithm-and-three-js
http://www.paulboxley.com/blog/2011/03/terrain-generation-mark-one
*/

//Generates a random real number in the range [-h, h]
float randRange(float h){
	return (rand() / (RAND_MAX / (h * 2))) - h;
}


//Executes the diamond step of the algorithm, setting the height of the midpoint of a square.
void TerrainMesh::makeDiamond(int x, int z, int sidelength, float h){
	float avg = (heights[x][z] + heights[x + sidelength][z] + heights[x][z + sidelength] + heights[x + sidelength][z + sidelength]) / 4; // average the four corners.
	int step = sidelength / 2;


	heights[x + step][z + step] = avg + randRange(h);
}


//Executes the square step of the algorithm. Takes provided x and z coordinates as the top corner of a diamond, and fills in the center point to make squares.

void TerrainMesh::makeSquare(int x, int z, int sidelength, float h){
	float a, b, c, d;
	int target_x, target_z;
	int step = sidelength / 2;


	target_x = x;
	target_z = z + step;


	a = heights[x][z];
	b = heights[target_x + step][target_z];

	if (z + sidelength >= terrain_size) c = heights[target_x][step];
	else c = heights[target_x][z + sidelength];


	if (x == 0) d = heights[terrain_size - step][target_z];
	else d = heights[x - step][target_z];


	float avg = (a + b + c + d) / 4; // average the four corners.
	float newHeight = avg + randRange(h);

	heights[target_x][target_z] = newHeight;

	if (target_x == 0) heights[terrain_size - 1][target_z] = newHeight; //for cells on an edge, mirror to opposing edge.
	if (target_z == terrain_size - 1) heights[target_x][0] = newHeight;

}

//Size should be 1 more than a power of 2. h determines the "ruggedness" of the terrain, with higher values leading to steeper peaks and valleys.

TerrainMesh::TerrainMesh(int size, float h){
	terrain_size = size;
	int sidelength = size - 1;
	heights.resize(size, std::vector<float>(size, 0.0)); // populates 2d vector at appropriate size.

	while (sidelength > 1){ //Each pass through the loop fills in the centers of all squares and diamonds, then restarts with a smaller size to generate finer detail.
		for (int i = 0; i + 1 < terrain_size; i += sidelength){
			for (int j = 0; j + 1 < terrain_size; j += sidelength){
				makeDiamond(i, j, sidelength, h);
			}
		}


		for (int i = 0; i + 1 < terrain_size; i += sidelength){
			for (int j = 0; j + 1 < terrain_size; j += sidelength){
				makeSquare(i, j, sidelength, h);
			}
		}

		for (int i = sidelength / 2; i < terrain_size; i += sidelength){
			for (int j = sidelength / 2; j < terrain_size; j += sidelength){
				makeSquare(i, j, sidelength, h);
			}
		}


		sidelength /= 2;
		h /= 2;

	}

}



// Outputs the algorithm's result as a 2d heightmap.

void TerrainMesh::mapToImg(){
	float min = 100000, max = -100000;


	for (int i = 0; i < terrain_size; i++){
		for (int j = 0; j < terrain_size; j++){
			if (heights[i][j] < min) min = heights[i][j];
			if (heights[i][j] > max) max = heights[i][j];
		}
	}

	SimpleImage img;
	img.initialize(terrain_size, terrain_size);

	for (int i = 0; i < terrain_size; i++){
		for (int j = 0; j < terrain_size; j++){
			float scale = (heights[i][j] - min) / (max - min);
			RGBColor c(scale, scale, scale);
			img.set(i, j, c);
		}
	}

	img.save("output.png");

}


// Takes the algorithm's result and converts it from a 2d array heightmap to a 3d mesh.

void TerrainMesh::mapToMesh(){
    const int width = terrain_size - 1;
    const int depth = terrain_size - 1;

    const float wdelta = 2.0f / width; // size delta for width
    const float ddelta = 2.0f / depth; // size delta for depth
                
    const float iDelta = (1.0f / (depth - 1)); // texture delta for depth
    const float jDelta = (1.0f / (width - 1)); // texture delta for width

	for (int i = 0; i  < depth; i++){
		for (int j = 0; j < width; j++){
            Vertex v;
            v.position = { 1.0f - wdelta*j, heights[j][i], 1.0f - ddelta*i };
            v.texCoord = { j * jDelta, i * iDelta };

            vertices.push_back(v);
		}
	}
    
	for (int i = 0; i  < depth-1; i++){
		for (int j = 0; j < width-1; j++){
            Face f1 = { j + i * width, j + (i+1) * width, (j+1) + i * width };
            Face f2 = { (j+1) + (i+1) * width, (j+1) + i * width, j + (i+1) * width };

            faces.push_back(f1);
            faces.push_back(f2);
		}
	}
    
    CalculateVertexNormals(vertices, faces);
}




