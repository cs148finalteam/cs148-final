
#pragma once

#include "OpenGLObject.h"

//---------------------------------------------------
// OpenGLBuffer
//
// Encapsulates an OpenGL buffer object
//---------------------------------------------------
class OpenGLBuffer : public OpenGLObject
{
    class CreateOnlyThroughFactory {};
public:

    static std::unique_ptr<OpenGLBuffer> Create(int size, const void* data);

    OpenGLBuffer(const CreateOnlyThroughFactory&, int size, const void* data);
    ~OpenGLBuffer();

    virtual GLenum GetObjectType() const override;
    virtual GLuint GetId() const override;

    void Bind(GLenum target);

private:
    GLuint m_id;
};

