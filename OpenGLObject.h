
#pragma once

#include <string>

#include "OpenGLUtil.h"

//---------------------------------------------------
// OpenGLObject
//
// Base class for all OpenGL objects.
//---------------------------------------------------
class OpenGLObject
{
public:

    OpenGLObject() = default;
    ~OpenGLObject() = default;

    OpenGLObject(const OpenGLObject&) = delete;
    const OpenGLObject& operator=(const OpenGLObject&) = delete;
    OpenGLObject(const OpenGLObject&&) = delete;
    const OpenGLObject& operator=(const OpenGLObject&&) = delete;

    void SetName(const std::string& name);
    
    virtual GLenum GetObjectType() const = 0;
    virtual GLuint GetId() const = 0;
};

