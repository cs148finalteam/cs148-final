
#pragma once

#include <iostream>
#include <fstream>
#include <memory>
#include <string>
#include <sstream>
#include <vector>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

//---------------------------------------------------
// Type Definitions
//---------------------------------------------------

using VertexPosition = glm::vec3;
using VertexNormal = glm::vec3;
using VertexTextureCoord = glm::vec2;

struct Vertex
{
    VertexPosition position;
    VertexNormal normal;
    VertexTextureCoord texCoord;
};

using Face = glm::uvec3;

//---------------------------------------------------
// Utilities
//---------------------------------------------------

// Calculates vertex normals for a mesh
void CalculateVertexNormals(std::vector<Vertex>& vertices, const std::vector<Face>& faces);

// Converts a position to a vertex
Vertex ToVertex(const VertexPosition& position);

//---------------------------------------------------
// Mesh
//
// Encapsulates geometry.
//---------------------------------------------------
struct Mesh
{
private:
	struct ConstructOnlyThroughFactory {};
public:
	static std::unique_ptr<Mesh> Create(std::string name, std::vector<Vertex> vertices, std::vector<Face> faces);

	Mesh(const ConstructOnlyThroughFactory&, std::string&& name, std::vector<Vertex>&& vertices, std::vector<Face>&& faces);

	const std::vector<Vertex>& Vertices() const;
	const int VertexCount() const;
	const int VertexBufferSize() const;
	const void* VertexBufferData() const;

    bool IsIndexed() const;
	const std::vector<Face>& Indices() const;
	const int IndexCount() const;
	const int IndexBufferSize() const;
	const void* IndexBufferData() const;

	const std::string& Name() const;

private:
    std::vector<Vertex> m_vertices;
    std::vector<Face> m_faces;
    std::string m_name;
};
