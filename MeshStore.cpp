
#include "MeshStore.h"
#include "OpenGLUtil.h"

void MeshStore::Add(std::string name, std::vector<Vertex> vertices, std::vector<Face> faces)
{
    auto spMesh = Mesh::Create(std::move(name), std::move(vertices), std::move(faces));
    DoAddMesh(std::move(spMesh));
}

std::shared_ptr<Mesh> MeshStore::Get(const std::string& name)
{
    auto it = m_meshes.find(name);
    if (it == m_meshes.end()) {
        return {};
    }

    return it->second;
}

void MeshStore::DoAddMesh(std::unique_ptr<Mesh> spMesh)
{
    const std::string& name = spMesh->Name();
    m_meshes[name] = ToShared(std::move(spMesh));
}
