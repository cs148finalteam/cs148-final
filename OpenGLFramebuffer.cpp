
#include "OpenGLFramebuffer.h"

std::unique_ptr<OpenGLFramebuffer> OpenGLFramebuffer::Create()
{
    return std::make_unique<OpenGLFramebuffer>(CreateOnlyThroughFactory());
}

OpenGLFramebuffer::OpenGLFramebuffer(const CreateOnlyThroughFactory&)
    : OpenGLObject()
    , m_id()
    , m_boundForRead(false)
    , m_boundForDraw(false)
{
    START_GL_OPERATIONS();
    
    GL_OP(glGenFramebuffers(1, &m_id));
}

OpenGLFramebuffer::~OpenGLFramebuffer()
{
    START_GL_OPERATIONS();

    GL_OP(glDeleteFramebuffers(1, &m_id));
}

GLenum OpenGLFramebuffer::GetObjectType() const
{
    return GL_FRAMEBUFFER;
}

GLuint OpenGLFramebuffer::GetId() const
{
    return m_id;
}

void OpenGLFramebuffer::Bind(GLenum target)
{
    START_GL_OPERATIONS();

    switch (target) {
    case GL_READ_FRAMEBUFFER:
        m_boundForRead = true;
        break;
    case GL_DRAW_FRAMEBUFFER:
        m_boundForDraw = true;
        break;
    case GL_FRAMEBUFFER:
        m_boundForRead = true;
        m_boundForDraw = true;
        break;
    }

    GL_OP(glBindFramebuffer(target, m_id));
}

void OpenGLFramebuffer::Unbind(GLenum target)
{
    START_GL_OPERATIONS();
    
    switch (target) {
    case GL_READ_FRAMEBUFFER:
        m_boundForRead = false;
        break;
    case GL_DRAW_FRAMEBUFFER:
        m_boundForDraw = false;
        break;
    case GL_FRAMEBUFFER:
        m_boundForRead = false;
        m_boundForDraw = false;
        break;
    }

    GL_OP(glBindFramebuffer(target, 0));
}
    
void OpenGLFramebuffer::Attach(const OpenGLTexture2D& texture, GLenum attachment)
{
    const auto target = (m_boundForRead) ? GL_READ_FRAMEBUFFER : GL_DRAW_FRAMEBUFFER;
    GL_OP(glFramebufferTexture2D(target, attachment, GL_TEXTURE_2D, texture.GetId(), 0));
}

