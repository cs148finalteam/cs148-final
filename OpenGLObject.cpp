
#pragma once

#include "OpenGLObject.h"

void OpenGLObject::SetName(const std::string& name)
{
    START_GL_OPERATIONS();

    if (glObjectLabel) {
        GL_OP(glObjectLabel(GetObjectType(), GetId(), -1, name.c_str()));
    }
}

