
#pragma once

#include "OpenGLObject.h"
#include "OpenGLBuffer.h"

//---------------------------------------------------
// OpenGLVertexArray
//
// Encapsulates an OpenGL vertex array.
//---------------------------------------------------
class OpenGLVertexArray : public OpenGLObject
{
    class CreateOnlyThroughFactory {};
public:

    static std::unique_ptr<OpenGLVertexArray> Create();

    OpenGLVertexArray(const CreateOnlyThroughFactory&);
    ~OpenGLVertexArray();
    
    virtual GLenum GetObjectType() const override;
    virtual GLuint GetId() const override;

    void Bind();
    void Unbind();

    void BeginVertexSpecification();
    void VertexAttribute(const OpenGLBuffer& buffer, int index, int size, GLenum type, int stride, int offset);
    void EndVertexSpecification();

private:
    GLuint m_id;
};

