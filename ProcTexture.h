
#include <glm/glm.hpp>
#include "TerrainMesh.h"
#include "OpenGLTexture.h"

//---------------------------------------------------
// CreateProcTexture
//
// Utilizes the Terrain Mesh algorithm to generate a texture. Maps the lowest and highest heights in the heightmaps to the given colors, and mixes them.
//---------------------------------------------------
std::unique_ptr<OpenGLTexture2D> CreateProcTexture(int size, glm::vec3 color1, glm::vec3 color2);