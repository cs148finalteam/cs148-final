#include "ProcTree.h"
#include <stdlib.h>
#include <time.h>

const int numGenerations = 5;

float rand0To1(){
	return float(rand()) / RAND_MAX;
}

float randVec(){
	return ((float)rand() / RAND_MAX - 0.5f) * 2.0f; // generates number between -1 and 1
}


//Generates a diamond-shaped leaf hanging down from the parent branch.
void ProcTree::leaf(glm::vec3 root){
	if (rand0To1() > leafChance) return;

	float angle = rand0To1();
	float inverse = 1.0f - angle;

	glm::vec3 b = { root.x + angle * height * 0.04, root.y - height * 0.02, root.z + inverse * height * 0.04}; //Leaves hang vertically, but their orientation in the x and z axes is random.
	glm::vec3 c = { root.x - angle * height * 0.04, root.y - height * 0.02, root.z - inverse * height * 0.04};
	glm::vec3 d = { root.x, root.y - height * 0.04, root.z };

	int newIndex = vertices.size();

    const int alternateFactor = 20;
    static int alternate = 0;
    float offset = (alternate) ? 0.0f : 0.45f;
    ++alternate;
    if (alternate >= alternateFactor) {
        alternate = 0;
    }

    vertices.emplace_back(Vertex{root, {}, { 0.25f+ offset, 1.0f  } });
	vertices.emplace_back(Vertex{b,    {}, { 0.1f + offset, 0.45f } });
    vertices.emplace_back(Vertex{c,    {}, { 0.45f+ offset, 0.45f } });
    vertices.emplace_back(Vertex{d,    {}, { 0.25f+ offset, 0.1f  } });

	Face f(newIndex, newIndex + 1, newIndex + 2);
	Face f2(newIndex+1, newIndex+2, newIndex+3);
	faces.emplace_back(f);
	faces.emplace_back(f2);
}

//recursively generates a fractal branching pattern
void ProcTree::branch(glm::vec3 root, glm::vec3 direction, int generation){
	if (generation >= numGenerations) {
		leaf(root);
		return;
	}

	float ratio = pow(branchRatio, generation); // scales the branch being created.
	float r = girth * ratio;
	glm::vec3 top(root + direction * height * ratio); // places vertex for the tip of the branch.

	int newIndex = vertices.size();

	glm::vec3 inverse(1.0 - direction.x, 1.0 - direction.y, 1.0 - direction.z);
	glm::vec3 normInverse = glm::normalize(inverse);
	glm::vec3 cross = glm::normalize(glm::cross(normInverse, direction));
	glm::vec3 baseDisp(r, r, r);

	// Places four vertices in a square pattern around the root point, along the plane perpendicular to the direction of the branch. 
	glm::vec3 b1(root + baseDisp * normInverse), b2(root + baseDisp * cross), b3(root - baseDisp * normInverse), b4(root - baseDisp * cross); 
	vertices.emplace_back(ToVertex(top));
	vertices.emplace_back(ToVertex(b1));
	vertices.emplace_back(ToVertex(b2));
	vertices.emplace_back(ToVertex(b3));
	vertices.emplace_back(ToVertex(b4));

	//Creates new faces from the new vertices to form the branch, in an elongated pyramid shape.
	Face f1(newIndex, newIndex + 1, newIndex + 2), f2(newIndex, newIndex + 2, newIndex + 3), f3(newIndex, newIndex + 3, newIndex + 4), f4(newIndex, newIndex + 4, newIndex + 1);
	faces.emplace_back(f1);
	faces.emplace_back(f2);
	faces.emplace_back(f3);
	faces.emplace_back(f4);

	//Runs along the length of the branch, checking each interval to see if a child branch should be spawned, and recursively calls itself if so.
	for (float i = branchStart * ratio; i < height * ratio; i += branchInterval * ratio){
		if (rand0To1() < branchChance){
			glm::vec3 randDir(randVec(), randVec(), randVec());
			randDir.y += 0.5; // branches should be more likely to slope upward.
			glm::vec3 normDir = glm::normalize(randDir);
			int genStep = 1;
			if (i > height * ratio * 0.5) genStep++; // avoid spawning large branches near the tips of parent branches.
			if (i > height * ratio * 0.75) genStep++; 
			branch(root + i * direction, normDir, generation + genStep); 
		}
	}
}

/*Constructs procedural tree using the provided parameters.
_startGirth: determines width of the tree at its base.
_height: determines the height of the tree.
_branchStart: determines how far along a parent branch child branches can start being generated.
_branchInterval: branch spawning is checked every interval above the branchStart point.
_branchChance: percentage chance that a branch will appear at each interval.
_branchRatio: Should be between 0 and 1, and sets how large each will be in relation to its parent.
_leafChance: When the final level of recursion is reached, a leaf will be generated instead of a branch. This scales how likely they are to spawn.
*/
ProcTree::ProcTree(float _startGirth, float _height, float _branchStart, float _branchInterval, float _branchChance, float _branchRatio, float _leafChance){
	girth = _startGirth;
	height = _height;
	branchStart = _branchStart;
	branchInterval = _branchInterval;
	branchChance = _branchChance;
	branchRatio = _branchRatio;
	leafChance = _leafChance;

	float xdir = rand0To1() / 2.5f - 0.2f; // Sets trunk to a slight angle instead of straight vertical.
	float zdir = rand0To1() / 2.5f - 0.2f;

	branch(glm::vec3(0.0, 0.0, 0.0), glm::normalize(glm::vec3(xdir, 1.0, zdir)), 0); //Starts a trunk at the origin and going upward.


}
