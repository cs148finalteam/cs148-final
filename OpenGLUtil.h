
#pragma once

#include <memory>

#include <GL/glew.h>
#include <GL/gl.h>

//---------------------------------------------------
// GL call validation
//---------------------------------------------------

#ifdef _DEBUG

#include <iostream>
#include <Windows.h>

#define BREAK_IF_DEBUGGING()   \
{                              \
    if (IsDebuggerPresent()) { \
        DebugBreak();          \
    }                          \
}

#define START_GL_OPERATIONS()                                                                            \
    {                                                                                                    \
        const auto err = glGetError();                                                                   \
        if (err) {                                                                                       \
            std::cerr << "Error at " << __FILE__ << ":" << __LINE__ << " - " << "Starting" << std::endl; \
            BREAK_IF_DEBUGGING();                                                                        \
        }                                                                                                \
    }

#define GL_OP(func)                                                                                 \
    func;                                                                                           \
    {                                                                                               \
        const auto err = glGetError();                                                              \
        if (err) {                                                                                  \
            std::cerr << "Error at " << __FILE__ << ":" << __LINE__ << " - " << #func << std::endl; \
            BREAK_IF_DEBUGGING();                                                                   \
        }                                                                                           \
    }

#define GL_OP_RET(ret, func)                                                                        \
    ret = func;                                                                                     \
    {                                                                                               \
        const auto err = glGetError();                                                              \
        if (err) {                                                                                  \
            std::cerr << "Error at " << __FILE__ << ":" << __LINE__ << " - " << #func << std::endl; \
            BREAK_IF_DEBUGGING();                                                                   \
        }                                                                                           \
    }

#define GLFW_OP(func) func

#else

#define BREAK_IF_DEBUGGING()
#define START_GL_OPERATIONS()
#define GL_OP(func) func
#define GLFW_OP(func) func
#define GL_OP_RET(ret, func) ret = func;

#endif

//---------------------------------------------------
// Types
//---------------------------------------------------

enum class ProjectionMode
{
    FullScreenQuad,
    Perspective,
    Count,
};

enum PolygonMode : int
{
	PolygonMode_Initial, 
	PolygonMode_Fill = PolygonMode_Initial, 
	PolygonMode_Line,
	PolygonMode_Point,
	PolygonMode_Count
};

//---------------------------------------------------
// Utilities
//---------------------------------------------------
template <typename T>
std::shared_ptr<T> ToShared(std::unique_ptr<T> spObject)
{
    return std::shared_ptr<T>(spObject.release());
}