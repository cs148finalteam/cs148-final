#include <vector>

#include "Mesh.h"

//---------------------------------------------------
// TerrainMesh
//
// Procedurally generates terrain mesh.
//---------------------------------------------------
class TerrainMesh{
public:
	TerrainMesh(int size, float h);
	void printMesh();
	void mapToImg();
	void mapToMesh();

	std::vector<std::vector<float>> heights;
	std::vector<Vertex> vertices;
	std::vector<Face> faces;
	int terrain_size; // length of each side of terrain to generate. Should be one more than a power of 2.

private:
	void makeDiamond(int i, int j, int sidelength, float h);
	void makeSquare(int i, int j, int sidelength, float h);
	void addFaces(int i);
};
