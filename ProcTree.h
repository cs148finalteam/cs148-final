#include <vector>
#include <glm/glm.hpp>
#include "Mesh.h"

//---------------------------------------------------
// ProcTree
//
// Procedurally generates a tree
//---------------------------------------------------

/*Parameters:
_startGirth: determines width of the tree at its base.
_height: determines the height of the tree.
_branchStart: determines how far along a parent branch child branches can start being generated.
_branchInterval: branch spawning is checked every interval above the branchStart point.
_branchChance: percentage chance that a branch will appear at each interval.
_branchRatio: Should be between 0 and 1, and sets how large each will be in relation to its parent.
_leafChance: When the final level of recursion is reached, a leaf will be generated instead of a branch. This scales how likely they are to spawn.
*/

class ProcTree{
public:
	std::vector<Vertex> vertices;
	std::vector<Face> faces;
	
	ProcTree(float startGirth, float height, float branchStart, float branchInterval, float branchChance, float branchRatio, float leafChance);

private:
	void branch(glm::vec3 root, glm::vec3 direction, int generation);
	void leaf(glm::vec3 root);
	
	float girth, height, branchStart, branchInterval, branchChance, branchRatio, leafChance;
};
