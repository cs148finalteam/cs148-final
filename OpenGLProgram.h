
#pragma once

#include <vector>

#include "OpenGLObject.h"
#include "OpenGLShader.h"

//---------------------------------------------------
// OpenGLProgram
//
// Encapsulates an OpenGL program object. It
// consumes the OpenGL shader object.
//---------------------------------------------------
class OpenGLProgram : public OpenGLObject
{
public:
    OpenGLProgram();
    ~OpenGLProgram();

    void BeginConstruction();
    void AddShader(GLenum type, const char* pSource);
    void AddShaderFromFile(GLenum type, const std::string& file);
    bool EndConstruction();

    void UseProgram() const;

    virtual GLenum GetObjectType() const override;
    virtual GLuint GetId() const override;

private:
    GLuint m_id;
    std::vector<std::unique_ptr<OpenGLShader>> m_shaders;
    GLint m_isLinked;
};
