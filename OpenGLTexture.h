
#pragma once

#include "OpenGLObject.h"

//---------------------------------------------------
// Types
//---------------------------------------------------
enum class WrapMode
{
    Clamp,
    Repeat,
};

//---------------------------------------------------
// OpenGLTexture2D
//
// Encapsulates an OpenGL 2D texture.
//---------------------------------------------------
class OpenGLTexture2D : public OpenGLObject
{
    class CreateOnlyThroughFactory {};
public:

    static std::unique_ptr<OpenGLTexture2D> Create(int width, int height, const void* data);

    OpenGLTexture2D(const CreateOnlyThroughFactory&, int width, int height, const void* data);
    ~OpenGLTexture2D();
    
    virtual GLenum GetObjectType() const override;
    virtual GLuint GetId() const override;

    void Bind() const;
    void SetWrapMode(WrapMode wrapMode);
    
    int GetWidth() const;
    int GetHeight() const;

private:
    GLuint m_id;
    int m_width;
    int m_height;
};

