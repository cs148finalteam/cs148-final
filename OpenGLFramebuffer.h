
#pragma once

#include "OpenGLObject.h"
#include "OpenGLTexture.h"

//---------------------------------------------------
// OpenGLBuffer
//
// Encapsulates an OpenGL framebuffer object
//---------------------------------------------------
class OpenGLFramebuffer : public OpenGLObject
{
    class CreateOnlyThroughFactory {};
public:

    static std::unique_ptr<OpenGLFramebuffer> Create();

    OpenGLFramebuffer(const CreateOnlyThroughFactory&);
    ~OpenGLFramebuffer();
    
    virtual GLenum GetObjectType() const override;
    virtual GLuint GetId() const override;

    void Bind(GLenum target);
    void Unbind(GLenum target);

    void Attach(const OpenGLTexture2D& texture, GLenum attachment);

private:
    GLuint m_id;
    bool m_boundForRead;
    bool m_boundForDraw;
};

