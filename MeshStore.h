
#pragma once

#include <map>

#include "Mesh.h"

//---------------------------------------------------
// MeshStore
//
// Provides a container for referencing meshes by
// name.
//---------------------------------------------------
class MeshStore
{
public:
	void Add(std::string name, std::vector<Vertex> vertices, std::vector<Face> faces);

    std::shared_ptr<Mesh> Get(const std::string& name);

private:
    void DoAddMesh(std::unique_ptr<Mesh> spMesh);

    std::map<std::string, std::shared_ptr<Mesh>> m_meshes;
};
