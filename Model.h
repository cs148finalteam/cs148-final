
#pragma once

#include <map>

#include "Mesh.h"

//---------------------------------------------------
// Forward declarations
//---------------------------------------------------
struct Mesh;
class OpenGLBuffer;
class OpenGLProgram;
class OpenGLTexture2D;
class OpenGLVertexArray;

enum class ProjectionMode;
enum PolygonMode : int;

//---------------------------------------------------
// Model
//
// Workhorse class for rendering a particular model.
// Provides transformations, settings, and
// a rendering method.
//---------------------------------------------------
__declspec( align( 16 ) ) class Model
{
public:
    Model(const OpenGLProgram& program, const std::shared_ptr<Mesh>& spMesh);
    
    Model(const Model& other) = default;
    Model& operator=(const Model& other) = default;
    ~Model() = default;

    void SetProjectionMode(ProjectionMode projectionMode);
    void SetPolygonMode(PolygonMode polygonMode);

    void BeginModelTransformation();
    void Rotate(float angle, glm::vec3 axis);
    void Scale(glm::vec3 scale);
    void Translate(glm::vec3 position);
    void EndModelTransformation();

    void SetTexture(int textureUnit, const OpenGLTexture2D* texture);

    void PrepareToRender();
    void Render();

    void* operator new(size_t i);
    void operator delete(void* p);

private:
    const OpenGLProgram& m_program;
    std::shared_ptr<Mesh> m_spMesh;
    std::unique_ptr<OpenGLBuffer> m_spVertexBuffer;
    std::unique_ptr<OpenGLBuffer> m_spIndexBuffer;
    std::unique_ptr<OpenGLVertexArray> m_spVao;

    ProjectionMode m_projectionMode;
    PolygonMode m_polygonMode;
    glm::mat4 m_model;
    std::map<int, const OpenGLTexture2D*> m_textures;
};

