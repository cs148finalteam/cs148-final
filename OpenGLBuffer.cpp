
#include "OpenGLBuffer.h"

std::unique_ptr<OpenGLBuffer> OpenGLBuffer::Create(int size, const void* data)
{
    return std::make_unique<OpenGLBuffer>(CreateOnlyThroughFactory(), size, data);
}

OpenGLBuffer::OpenGLBuffer(const CreateOnlyThroughFactory&, int size, const void* data)
    : OpenGLObject()
    , m_id()
{
    START_GL_OPERATIONS();

#if 0
    if (glCreateBuffers) {
        GL_OP(glCreateBuffers(1, &m_id));
        GL_OP(glNamedBufferStorage(m_id, size, data, GL_DYNAMIC_STORAGE_BIT));
    } else
#endif
    {
        GL_OP(glGenBuffers(1, &m_id));
        GL_OP(glBindBuffer(GL_COPY_READ_BUFFER, m_id));
        GL_OP(glBufferData(GL_COPY_READ_BUFFER, size, data, GL_STATIC_DRAW));
    }
}

OpenGLBuffer::~OpenGLBuffer()
{
    START_GL_OPERATIONS();

    GL_OP(glDeleteBuffers(1, &m_id));
}

GLenum OpenGLBuffer::GetObjectType() const
{
    return GL_BUFFER;
}

GLuint OpenGLBuffer::GetId() const
{
    return m_id;
}

void OpenGLBuffer::Bind(GLenum target)
{
    START_GL_OPERATIONS();

    GL_OP(glBindBuffer(target, m_id));
}
