
#include "Mesh.h"

void CalculateVertexNormals(std::vector<Vertex>& vertices, const std::vector<Face>& faces)
{
    const auto numFaces = faces.size();
    std::vector<glm::vec3> faceNormals(numFaces);

    for (size_t i = 0; i < numFaces; ++i) {
        const auto& face = faces[i];
        
        // Calculate a normal
		const glm::vec3 x0(vertices[face.x].position);
		const glm::vec3 x1(vertices[face.y].position);
		const glm::vec3 x2(vertices[face.z].position);

		const auto u = x1 - x0;
		const auto v = x2 - x0;

		const auto cp = glm::cross(u, v);
		const auto norm = glm::normalize(cp);
        
        faceNormals[i] = norm;
    }
    
    // Add all face normals to each vertex
    for (size_t i = 0; i < numFaces; ++i) {
        const auto& face = faces[i];
        
		vertices[face.x].normal += faceNormals[i];
		vertices[face.y].normal += faceNormals[i];
		vertices[face.z].normal += faceNormals[i];
    }
    
    // Normalize each vertex normal
    const auto numVertices = vertices.size();
    for (size_t i = 0; i < numVertices; ++i) {
        auto& vertex = vertices[i];
        vertex.normal = glm::normalize(vertex.normal);
    }
}

Vertex ToVertex(const VertexPosition& position)
{
    Vertex v;
    v.position = position;
    return v;
}

Mesh::Mesh(const ConstructOnlyThroughFactory&, std::string&& name, std::vector<Vertex>&& vertices, std::vector<Face>&& faces)
	: m_vertices(std::move(vertices))
    , m_faces(std::move(faces))
    , m_name(std::move(name))
{
}

const std::vector<Vertex>& Mesh::Vertices() const
{
	return m_vertices;
}

const int Mesh::VertexCount() const
{
	return static_cast<int>(Vertices().size());
}

const int Mesh::VertexBufferSize() const
{
	return VertexCount() * sizeof(Vertex);
}

const void* Mesh::VertexBufferData() const
{
	return m_vertices.data();
}

bool Mesh::IsIndexed() const
{
    return !m_faces.empty();
}

const std::vector<Face>& Mesh::Indices() const
{
	return m_faces;
}

const int Mesh::IndexCount() const
{
	return static_cast<int>(Indices().size() * 3);
}

const int Mesh::IndexBufferSize() const
{
	return IndexCount() * sizeof(Face::value_type);
}

const void* Mesh::IndexBufferData() const
{
	return m_faces.data();
}

const std::string& Mesh::Name() const
{
	return m_name;
}

std::unique_ptr<Mesh> Mesh::Create(std::string name, std::vector<Vertex> vertices, std::vector<Face> faces)
{
    return std::make_unique<Mesh>(ConstructOnlyThroughFactory(), std::move(name), std::move(vertices), std::move(faces));
}
