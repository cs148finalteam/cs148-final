
#include "OpenGLVertexArray.h"

std::unique_ptr<OpenGLVertexArray> OpenGLVertexArray::Create()
{
    return std::make_unique<OpenGLVertexArray>(CreateOnlyThroughFactory());
}

OpenGLVertexArray::OpenGLVertexArray(const CreateOnlyThroughFactory&)
    : OpenGLObject()
    , m_id()
{
    START_GL_OPERATIONS();

#if 0
    GL_OP(glCreateVertexArrays(1, &m_id));
#else
    GL_OP(glGenVertexArrays(1, &m_id));
#endif
}

OpenGLVertexArray::~OpenGLVertexArray()
{
    START_GL_OPERATIONS();

    GL_OP(glDeleteVertexArrays(1, &m_id));
}

GLenum OpenGLVertexArray::GetObjectType() const
{
    return GL_VERTEX_ARRAY;
}

GLuint OpenGLVertexArray::GetId() const
{
    return m_id;
}

void OpenGLVertexArray::Bind()
{
    START_GL_OPERATIONS();

    GL_OP(glBindVertexArray(m_id));
}

void OpenGLVertexArray::Unbind()
{
    START_GL_OPERATIONS();

    GL_OP(glBindVertexArray(0));
}

void OpenGLVertexArray::BeginVertexSpecification()
{
    Bind();
}

void OpenGLVertexArray::VertexAttribute(const OpenGLBuffer& buffer, int index, int size, GLenum type, int stride, int offset)
{
    START_GL_OPERATIONS();

    GL_OP(glBindBuffer(GL_ARRAY_BUFFER, buffer.GetId()));
    GL_OP(glEnableVertexAttribArray(index));
    GL_OP(glVertexAttribPointer(index, size, type, GL_FALSE, stride, reinterpret_cast<const void*>(offset)));
}

void OpenGLVertexArray::EndVertexSpecification()
{
    Unbind();
}
