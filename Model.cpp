
#include "Model.h"

#include "OpenGLBuffer.h"
#include "OpenGLProgram.h"
#include "OpenGLTexture.h"
#include "OpenGLVertexArray.h"

extern void UseProgram(const OpenGLProgram& program);
extern void SetProjectionMode(ProjectionMode projectionMode);
extern void SetPolygonMode(PolygonMode polygonMode);
extern void UpdateModel(const glm::mat4& model);

Model::Model(const OpenGLProgram& program, const std::shared_ptr<Mesh>& spMesh)
    : m_program(program)
    , m_spMesh(spMesh)
    , m_spVertexBuffer()
    , m_spIndexBuffer()
    , m_projectionMode(ProjectionMode::Perspective)
    , m_polygonMode(PolygonMode_Initial)
    , m_model()
{
    m_spVertexBuffer = OpenGLBuffer::Create(m_spMesh->VertexBufferSize(), m_spMesh->VertexBufferData());
    m_spVertexBuffer->SetName(spMesh->Name());

    m_spVao = OpenGLVertexArray::Create();

    if (spMesh->IsIndexed()) {
        m_spIndexBuffer = OpenGLBuffer::Create(m_spMesh->IndexBufferSize(), m_spMesh->IndexBufferData());
        m_spIndexBuffer->SetName(spMesh->Name() + " (indices)");
        m_spIndexBuffer->Bind(GL_ELEMENT_ARRAY_BUFFER);
    }

    m_spVao->BeginVertexSpecification();
        m_spVao->VertexAttribute(*m_spVertexBuffer, 0, 3, GL_FLOAT, sizeof(Vertex), offsetof(Vertex, position));      
        m_spVao->VertexAttribute(*m_spVertexBuffer, 1, 3, GL_FLOAT, sizeof(Vertex), offsetof(Vertex, normal));       
        m_spVao->VertexAttribute(*m_spVertexBuffer, 2, 2, GL_FLOAT, sizeof(Vertex), offsetof(Vertex, texCoord));
    m_spVao->EndVertexSpecification();
}

void Model::SetProjectionMode(ProjectionMode projectionMode)
{
    m_projectionMode = projectionMode;
}

void Model::SetPolygonMode(PolygonMode polygonMode)
{
    m_polygonMode = polygonMode;
}

void Model::BeginModelTransformation()
{
    m_model = {};
}

void Model::Rotate(float angle, glm::vec3 axis)
{
    m_model = glm::rotate(glm::mat4(1.0f), angle, axis) * m_model;
}

void Model::Scale(glm::vec3 scale)
{
    m_model = glm::scale(glm::mat4(1.0f), scale) * m_model;
}

void Model::Translate(glm::vec3 position)
{
    m_model = glm::translate(glm::mat4(1.0f), position) * m_model;
}

void Model::EndModelTransformation()
{
}

void Model::SetTexture(int textureUnit, const OpenGLTexture2D* pTexture)
{
    if (!pTexture) {
        m_textures.erase(textureUnit);
    } else {
        m_textures[textureUnit] = pTexture;
    }
}

void Model::PrepareToRender()
{
    UseProgram(m_program);
}

void Model::Render()
{
    START_GL_OPERATIONS();

    UseProgram(m_program);
    ::SetProjectionMode(m_projectionMode);
    UpdateModel(m_model);

    for (const auto& tex : m_textures) {
        GL_OP(glActiveTexture(GL_TEXTURE0 + tex.first));
        tex.second->Bind();
    }
    
    ::SetPolygonMode(m_polygonMode);
    m_spVao->Bind();
    if (m_spMesh->IsIndexed()) {
        if (m_spIndexBuffer) {
            m_spIndexBuffer->Bind(GL_ELEMENT_ARRAY_BUFFER);
        }

        GL_OP(glDrawElements(GL_TRIANGLES, m_spMesh->IndexCount(), GL_UNSIGNED_INT, 0));
    } else {
        GL_OP(glDrawArrays(GL_TRIANGLES, 0, m_spMesh->VertexCount()));
    }
}

void* Model::operator new(size_t i)
{
    return _mm_malloc(i, 16);
}

void Model::operator delete(void* p)
{
    _mm_free(p);
}
