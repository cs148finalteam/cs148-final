// Dan Price
// Gareth Hughes
// Final Project

#include <algorithm>
#include <cassert>
#include <cstddef>
#include <ctime>
#include <fstream>
#include <iostream>
#include <map>
#include <memory>
#include <random>
#include <string>
#include <sstream>
#include <vector>

#if defined(_WIN32)
#include <Windows.h>
#endif

#include <GL/glew.h>
#include <GL/gl.h>

#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "OpenGLBuffer.h"
#include "OpenGLFramebuffer.h"
#include "OpenGLShader.h"
#include "OpenGLProgram.h"
#include "OpenGLTexture.h"
#include "OpenGLVertexArray.h"
#include "Mesh.h"
#include "Model.h"
#include "MeshStore.h"
#include "SimpleImage.h"
#include "ProcTexture.h"
#include "ProcTree.h"

//---------------------------------------------------
// Global data
//---------------------------------------------------

// Window
static int g_windowWidth = 1280;
static int g_windowHeight = 960;

// Matrices
static glm::vec3 g_translation = {};
static glm::vec3 g_rotation = {};
static glm::mat4 g_model;
static glm::mat4 g_view;
static glm::mat4 g_projection;

// Uniforms
static int g_locColor = -1;
static int g_locTexOffset = -1;
static glm::vec2 g_texOffset = {};

// Input control
static int g_buttonState[GLFW_MOUSE_BUTTON_LAST];
static double g_lastPositionX = 0.0;
static double g_lastPositionY = 0.0;

// OpenGL State
static ProjectionMode g_activeProjectionMode = ProjectionMode::Count;
static const OpenGLProgram* g_pActiveProgram = nullptr;
static OpenGLProgram* pLightingProgram = nullptr;
static PolygonMode g_activePolygonMode = PolygonMode_Initial;
static PolygonMode g_activeModelPolygonMode = PolygonMode_Initial;
static const GLenum g_polygonModeEnums[PolygonMode_Count] =
{
	GL_FILL,
	GL_LINE,
	GL_POINT,
};

// Terrain generation
#ifdef _DEBUG
static const int g_terrainSize = 128;
#else
static const int g_terrainSize = 1024;
#endif
static glm::vec3 g_terrainScaleDefault = { 2.0f, 1.0f, 2.0f };
static glm::vec3 g_terrainScale = g_terrainScaleDefault;

static const std::vector<float> g_ruggedness = { 0.4f, 1.0f, 3.0f };
static int g_ruggedIndex = 1;

// Sky generation
static const std::vector<glm::vec3> g_skyColor1 = { { 1.0, 1.0, 1.0 }, { 1.0, 0.45, 0.85 }, { 0.27, 0.27, 0.4 } };
static const std::vector<glm::vec3> g_skyColor2 = { { 0.0, 0.2, 0.9 }, { 1.0, 0.55, 0.15 }, { 0.63, 0.48, 0.0 } };
static int g_skyColorIndex = 0;

// Ground texture generation
static const std::vector<glm::vec3> g_groundColor1 = { { 0.4, 0.2, 0.0  }, { 0.8, 0.8, 0.0 }, { 0.8, 0.0, 0.0  }, { 0.0, 0.0, 0.0 } };
static const std::vector<glm::vec3> g_groundColor2 = { { 0.0, 0.8, 0.15 }, { 0.9, 0.6, 0.3 }, { 0.0, 0.0, 0.25 }, { 0.4, 0.4, 0.4 } };
static int g_groundColorIndex = 0;

// Tree generation
static const int g_numTreeModels = 4;
static int g_numTreeInstances = 131;
static int g_treeModelPreset = 0;

// Regeneration
static bool g_regenTerrainMesh = true;
static bool g_regenTerrainTexture = true;
static bool g_regenSkyTexture = true;
static bool g_regenTrees = true;
static bool g_regenTreeModels = true;

// Drawing control
static bool g_drawTrees = true;
static bool g_drawSun = false;
static bool g_animateSky = false;

//---------------------------------------------------
// Lighting
//---------------------------------------------------
glm::vec4 LightPosition;
glm::vec4 g_ambient = { 0.35f, 0.35f, 0.35f, 1.0f };
glm::vec4 g_diffuse = { 0.2f, 0.2f, 0.2f, 1.0f };
glm::vec3 Kd = glm::vec3(.5f, .5f, .5f);
glm::vec3 Ld = glm::vec3(1.0f, 1.0f, 1.0f);

//---------------------------------------------------
// View Control
//---------------------------------------------------
void UpdateMVP()
{
    const auto MV = g_view * g_model;
    const auto MVP = g_projection * MV;
    const auto Normal = glm::mat3( glm::vec3(MV[0]), glm::vec3(MV[1]), glm::vec3(MV[2]) );

    int locNormal = 0;
    GL_OP_RET(locNormal, glGetUniformLocation(g_pActiveProgram->GetId(), "NormalMatrix"));
    if (locNormal != -1) {
        GL_OP(glUniformMatrix3fv(locNormal, 1, GL_FALSE, &Normal[0][0]));
    }

    int locMV = 0;
    GL_OP_RET(locMV, glGetUniformLocation(g_pActiveProgram->GetId(), "ModelViewMatrix"));
    if (locMV != -1) {
        GL_OP(glUniformMatrix4fv(locMV, 1, GL_FALSE, &MV[0][0]));
    }

    int locP = 0;
    GL_OP_RET(locP, glGetUniformLocation(g_pActiveProgram->GetId(), "ProjectionMatrix"));
    if (locP != -1) {
        GL_OP(glUniformMatrix4fv(locP, 1, GL_FALSE, &g_projection[0][0]));
    }

    int locMVP = 0;
    GL_OP_RET(locMVP, glGetUniformLocation(g_pActiveProgram->GetId(), "MVP"));
    if (locMVP != -1) {
        GL_OP(glUniformMatrix4fv(locMVP, 1, GL_FALSE, &MVP[0][0]));
    }
}

void UpdateModel(const glm::mat4& model)
{
    g_model = model;

    UpdateMVP();
}

void UpdateView()
{
    glm::mat4 ViewTranslate = glm::translate(glm::mat4(1.0f), glm::vec3(g_translation.x, g_translation.y, g_translation.z));
    glm::mat4 ViewRotateX = glm::rotate(ViewTranslate, g_rotation.y, glm::vec3(1.f, 0.f, 0.f));
    glm::mat4 ViewRotateY = glm::rotate(ViewRotateX, g_rotation.x, glm::vec3(0.f, 1.f, 0.f));
    glm::mat4 View = glm::rotate(ViewRotateY, g_rotation.z, glm::vec3(0.f, -1.f, 1.f));
    g_view = View;

    UpdateMVP();
}

void UpdateProjection()
{
    g_projection = glm::perspective(30.0f, float(g_windowWidth) / g_windowHeight, 0.001f, 100.0f);

    UpdateMVP();
}

void ResetCamera()
{
    g_translation = glm::vec3(0.0f, 0.1f, -4.5f);
    g_rotation = glm::vec3(-0.6f, 2.4f, 0.0f);

    UpdateView();
}

void SetProjectionMode(ProjectionMode mode)
{
    if (g_activeProjectionMode == mode) {
        return;
    }

    switch (mode) {
    case ProjectionMode::FullScreenQuad:
        g_view = {};
        g_model = {};
        g_projection = {};
        UpdateMVP();
        break;
    case ProjectionMode::Perspective:
        UpdateView();
        UpdateProjection();
        break;
    default:
        std::cerr << "Unknown mode: " << static_cast<int>(mode) << std::endl;
        return;
    }

    g_activeProjectionMode = mode;
}

//---------------------------------------------------
// GL State
//---------------------------------------------------

void SetPolygonMode(PolygonMode polygonMode)
{
    if (g_activePolygonMode == polygonMode) {
        return;
    }

    START_GL_OPERATIONS();

	GL_OP(glPolygonMode(GL_FRONT_AND_BACK, g_polygonModeEnums[polygonMode]));
    g_activePolygonMode = polygonMode;
}

void UseProgram(const OpenGLProgram& program)
{
    if (g_pActiveProgram == &program) {
        return;
    }

    program.UseProgram();
    g_pActiveProgram = &program;
}

//---------------------------------------------------
// Lighting
//---------------------------------------------------

void UpdateLighting(const OpenGLProgram& program)
{
    UseProgram(program);

    using UniformSetter = void(int);
    auto SetIfPresent = [&program](const char* pName, UniformSetter pFn)
    {
        const auto location = glGetUniformLocation(program.GetId(), pName);
        if (location != -1) {
            pFn(location);
        }
    };

    const auto MV = g_view * g_model;
    const auto MVP = g_projection * MV;
    static glm::vec4 LightPositionInEyeCoords;
    LightPositionInEyeCoords = g_view * LightPosition;

    SetIfPresent("LightPosition", [] (int location) { GL_OP(glUniform4fv(location, 1, &LightPositionInEyeCoords[0])); });
    SetIfPresent("AmbientPower", [] (int location) { GL_OP(glUniform4fv(location, 1, &g_ambient[0])); });
    SetIfPresent("DiffusePower", [] (int location) { GL_OP(glUniform4fv(location, 1, &g_diffuse[0])); });
    SetIfPresent("Kd", [] (int location) { GL_OP(glUniform4fv(location, 1, &Kd[0])); });
    SetIfPresent("Ld", [] (int location) { GL_OP(glUniform3fv(location, 1, &Ld[0])); });
}

void InitializeLighting(const OpenGLProgram& program)
{
	LightPosition = glm::vec4(0.0f, 5.0f, 0.0f, 1.0f);
	UpdateLighting(program);
}

void ClampToZero(float& val)
{
	val = std::max(0.0f, val);
}

void DoAdjustLight(glm::vec4& light, float adjustment)
{
	light.x += adjustment;
	ClampToZero(light.x);
	light.z = light.y = light.x;

    UpdateLighting(*pLightingProgram);
}

void AdjustAmbientLight(float adjustment)
{
	DoAdjustLight(g_ambient, adjustment);
}

void AdjustDiffuseLight(float adjustment)
{
	DoAdjustLight(g_diffuse, adjustment);
}

//---------------------------------------------------
// Utilities
//---------------------------------------------------
std::unique_ptr<OpenGLTexture2D> CreateTextureFromFile(const char* pFile)
{
    SimpleImage texPNG(pFile);

    auto ret = OpenGLTexture2D::Create(texPNG.width(), texPNG.height(), texPNG.data());
    ret->SetName(pFile);
    return ret;
}

//---------------------------------------------------
// Callbacks
//---------------------------------------------------

static void OnKey(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    switch (key) {
    case GLFW_KEY_W:
        g_translation.z += .1f;
        UpdateView();
        break;
    case GLFW_KEY_S:
        g_translation.z -= .1f;
        UpdateView();
        break;
    case GLFW_KEY_A:
        g_translation.x -= .1f;
        UpdateView();
        break;
    case GLFW_KEY_D:
        g_translation.x += .1f;
        UpdateView();
        break;
    case GLFW_KEY_Q:
        g_translation.y -= .1f;
        UpdateView();
        break;
    case GLFW_KEY_E:
        g_translation.y += .1f;
        UpdateView();
        break;
    case GLFW_KEY_R:
        g_rotation.y -= .1f;
        UpdateView();
        break;
    case GLFW_KEY_F:
        g_rotation.y += .1f;
        UpdateView();
        break;
    case GLFW_KEY_T:
        if (mods & GLFW_MOD_CONTROL) {
            if (action == GLFW_PRESS) {
                g_drawTrees = !g_drawTrees;
            }
        } else if (mods & GLFW_MOD_ALT) {
            if (action == GLFW_PRESS) {
                ++g_treeModelPreset;
                g_regenTreeModels = true;
            }
        } else {
            if (mods & GLFW_MOD_SHIFT) {
                g_numTreeInstances -= 2;
                g_numTreeInstances = std::max(1, g_numTreeInstances);
            } else {
                g_numTreeInstances += 2;
            }
            g_regenTrees = true;
        }
        break;
    case GLFW_KEY_Y:
        if (mods & GLFW_MOD_ALT) {
            g_terrainScale = g_terrainScaleDefault;
        } else if (mods & GLFW_MOD_CONTROL) {
            if (mods & GLFW_MOD_SHIFT) {
                g_terrainScale.x -= .015f;
                g_terrainScale.z -= .015f;
            } else {
                g_terrainScale.x += .015f;
                g_terrainScale.z += .015f;
            }
        } else {
            if (mods & GLFW_MOD_SHIFT) {
                g_terrainScale.y -= .01f;
            } else {
                g_terrainScale.y += .01f;
            }
        }
        break;
    case GLFW_KEY_U:
        if (action == GLFW_PRESS) {
            if (mods & GLFW_MOD_SHIFT) {
				g_ruggedIndex++;
				if (g_ruggedIndex >= static_cast<int>(g_ruggedness.size())) g_ruggedIndex = 0;
            }
            g_regenTerrainMesh = true;
        }
        break;
    case GLFW_KEY_I:
        if (action == GLFW_PRESS) {
            if (mods & GLFW_MOD_SHIFT) {
				g_groundColorIndex++;
				if (g_groundColorIndex >= static_cast<int>(g_groundColor1.size())) g_groundColorIndex = 0;
            }
            g_regenTerrainTexture = true;
        }
        break;
    case GLFW_KEY_K:
        if (action == GLFW_PRESS) {
            if ((mods & (GLFW_MOD_SHIFT|GLFW_MOD_CONTROL)) == (GLFW_MOD_SHIFT|GLFW_MOD_CONTROL)) {
                g_animateSky = !g_animateSky;
            } else {
                if (mods & GLFW_MOD_SHIFT) {
				    g_skyColorIndex++;
				    if (g_skyColorIndex >= static_cast<int>(g_skyColor1.size())) g_skyColorIndex = 0;
                }
                g_regenSkyTexture = true;
            }
        }
        break;
    case GLFW_KEY_O:
        if (action == GLFW_PRESS) {
            g_drawSun = !g_drawSun;
        }
        break;
    case GLFW_KEY_COMMA:
        if (mods & GLFW_MOD_SHIFT) {
		    AdjustAmbientLight(-0.01f);
        } else {
		    AdjustAmbientLight(+0.01f);
        }
		break;
    case GLFW_KEY_PERIOD:
        if (mods & GLFW_MOD_SHIFT) {
		    AdjustDiffuseLight(-0.01f);
        } else {
		    AdjustDiffuseLight(+0.01f);
        }
		break;
    case GLFW_KEY_P:
        if (action == GLFW_PRESS) {
		    g_activeModelPolygonMode = static_cast<PolygonMode>(static_cast<int>(g_activeModelPolygonMode) + 1);
		    if (g_activeModelPolygonMode == PolygonMode_Count) {
			    g_activeModelPolygonMode = PolygonMode_Initial;
		    }
        }
		break;
    case GLFW_KEY_V:
        if (action == GLFW_PRESS) {
            ResetCamera();
        }
        break;
    }
}

static void OnMouseButton(GLFWwindow* window, int button, int action, int mods)
{
    g_buttonState[button] = action;
    glfwGetCursorPos(window, &g_lastPositionX, &g_lastPositionY);
}

static void OnScroll(GLFWwindow* window, double xOffset, double yOffset)
{
}

static void OnCursor(GLFWwindow* window, double xPos, double yPos)
{
    if (g_lastPositionX != 0.0 && g_lastPositionY != 0.0) {
        if (g_buttonState[GLFW_MOUSE_BUTTON_1]) {
            g_rotation.x -= float(xPos - g_lastPositionX) / 100;
            g_rotation.y += float(yPos - g_lastPositionY) / 100;

            UpdateView();
        } else if (g_buttonState[GLFW_MOUSE_BUTTON_2]) {
            LightPosition.x -= float(xPos - g_lastPositionX) / 100;
            LightPosition.z += float(yPos - g_lastPositionY) / 100;
            UpdateLighting(*pLightingProgram);
        } else if (g_buttonState[GLFW_MOUSE_BUTTON_3]) {
            LightPosition.y += (float(xPos - g_lastPositionX) + float(yPos - g_lastPositionY)) / 100;
            UpdateLighting(*pLightingProgram);
        }
    }

    g_lastPositionX = xPos;
    g_lastPositionY = yPos;
}

static void OnResize(GLFWwindow* window, int width, int height)
{
    g_windowWidth = width;
    g_windowHeight = height;

    GL_OP(glViewport(0, 0, g_windowWidth, g_windowHeight));
    GL_OP(glScissor(0, 0, g_windowWidth, g_windowHeight));

    UpdateProjection();
}

static void GLAPIENTRY OnDebugInfo(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam)
{
    std::cerr << message;
}

//---------------------------------------------------
// Main program
//---------------------------------------------------

void Render(GLFWwindow* pWindow)
{
    START_GL_OPERATIONS();

    const char* pVertexShaderQuadTexturing = R"RAW(
        #version 400

        layout(location = 0) in vec3 in_vp;
        layout(location = 1) in vec3 in_vn;
        layout(location = 2) in vec2 in_texCoord;

        out vec2 texCoord;

        uniform mat4 MVP;

        void main()
        {
            texCoord = in_texCoord;
            gl_Position = MVP * vec4(in_vp, 1);
        }

    )RAW";

    const char* pFragmentShaderQuadTexturing = R"RAW(
        #version 400

        in vec2 texCoord;

        out vec4 FragColor;

        uniform sampler2D tex;
        uniform vec2 texOffset;

        void main()
        {
            vec4 texColor = texture(tex, texCoord + texOffset);
            FragColor = texColor;
        }

    )RAW";

    const char* pVertexShaderPerFragmentLighting = R"RAW(
        #version 400

        layout(location = 0) in vec3 in_vp;
        layout(location = 1) in vec3 in_vn;
        layout(location = 2) in vec2 in_texCoord;

        out vec2 texCoord;
		out vec3 Position;
		out vec3 Normal;

		uniform mat4 ModelViewMatrix;
		uniform mat3 NormalMatrix;
		uniform mat4 ProjectionMatrix;
        uniform mat4 MVP;

        void main()
        {
            texCoord = in_texCoord;

            Position = vec3(ModelViewMatrix * vec4(in_vp, 1));
            Normal = normalize(NormalMatrix * in_vn);

            gl_Position = MVP * vec4(in_vp, 1);
        }

    )RAW";

    const char* pFragmentShaderPerFragmentLighting = R"RAW(
        #version 400

        uniform vec4 Kd;
        uniform vec4 Ks;

		uniform vec4 LightPosition;
		uniform vec4 AmbientPower;
		uniform vec4 DiffusePower;
        uniform sampler2D tex;

        in vec2 texCoord;
        in vec3 Normal;
        in vec3 Position;

        out vec4 FragColor;

        void main(void)
        {
           // Ambient
           vec4 ambient = AmbientPower;

           // Diffuse
           vec3 l = normalize(LightPosition.xyz - Position);
           vec4 diffuse = Kd * DiffusePower * max(0.0, dot(Normal, l));
           diffuse = clamp(diffuse, 0.0, 1.0);

           // Combine components
           FragColor = (ambient + diffuse) * texture(tex, texCoord);
        }

    )RAW";

    OpenGLProgram programTexturing;
    OpenGLProgram programFragmentLighting;

    // Construct programs
    {
        // Fragment lighting
        {
            programFragmentLighting.BeginConstruction();
            programFragmentLighting.AddShader(GL_VERTEX_SHADER, pVertexShaderPerFragmentLighting);
            programFragmentLighting.AddShader(GL_FRAGMENT_SHADER, pFragmentShaderPerFragmentLighting);
            programFragmentLighting.EndConstruction();

            UseProgram(programFragmentLighting);
            InitializeLighting(programFragmentLighting);
            GL_OP_RET(g_locColor, glGetUniformLocation(programFragmentLighting.GetId(), "Color"));
            {
                const int g_locTex = glGetUniformLocation(programFragmentLighting.GetId(), "tex");
                GL_OP(glUniform1i(g_locTex, 0));
            }
            pLightingProgram = &programFragmentLighting;
        }

        // Full screen program
        {
            programTexturing.BeginConstruction();
            programTexturing.AddShader(GL_VERTEX_SHADER, pVertexShaderQuadTexturing);
            programTexturing.AddShader(GL_FRAGMENT_SHADER, pFragmentShaderQuadTexturing);
            programTexturing.EndConstruction();

            UseProgram(programTexturing);
            {
                const int g_locTex = glGetUniformLocation(programTexturing.GetId(), "tex");
                GL_OP(glUniform1i(g_locTex, 0));
            }
            GL_OP_RET(g_locTexOffset, glGetUniformLocation(programTexturing.GetId(), "texOffset"));
        }
    }

    // Construct Meshes
    MeshStore meshStore;
    {
        std::vector<Vertex> fullScreenQuad =
        {
            { {+1.0f, +1.0f, +0.0f }, { 0.0f, 0.0f, 0.0f }, { 1.0, 1.0 } },
            { {+1.0f, -1.0f, +0.0f }, { 0.0f, 0.0f, 0.0f }, { 1.0, 0.0 } },
            { {-1.0f, +1.0f, +0.0f }, { 0.0f, 0.0f, 0.0f }, { 0.0, 1.0 } },
            { {-1.0f, -1.0f, +0.0f }, { 0.0f, 0.0f, 0.0f }, { 0.0, 0.0 } },
        };

        std::vector<Face> quadFaces =
        {
            { { 0, 1, 2 } },
            { { 3, 2, 1 } },
        };

        meshStore.Add("fullscreenquad", fullScreenQuad, quadFaces);

        std::vector<Vertex> triangle =
        {
            { {+0.0f, -0.5f, 0.0f}, { 0.0f, 0.0f, 0.0f }, { 1.0, 1.0 } },
            { {+0.5f, -1.0f, 0.0f}, { 0.0f, 0.0f, 0.0f }, { 1.0, 0.0 } },
            { {-0.5f, -1.0f, 0.0f}, { 0.0f, 0.0f, 0.0f }, { 0.0, 1.0 } },
        };
        std::vector<Face> triangleFaces =
        {
            { { 0, 1, 2 } },
        };

        meshStore.Add("triangle", triangle, triangleFaces);
    }
    auto spFullScreenQuadMesh = meshStore.Get("fullscreenquad");
    auto spTriangleMesh = meshStore.Get("triangle");

    // Textures
    auto spSunTexture = CreateTextureFromFile("resources/sun.png");
    auto spLeafTexture = CreateTextureFromFile("resources/leaf.png");
    std::unique_ptr<OpenGLTexture2D> spSkyTexture;
    std::unique_ptr<OpenGLTexture2D> spGroundTexture;

    // Models
    std::unique_ptr<Model> spTerrainDynamic;

    Model sky { programTexturing, spFullScreenQuadMesh };
    sky.SetProjectionMode(ProjectionMode::FullScreenQuad);

    Model sun { programTexturing, spTriangleMesh };
    sun.SetTexture(0, spSunTexture.get());

    std::shared_ptr<Model> trees[g_numTreeModels];

    struct TreeInstance
    {
        std::shared_ptr<Model> spModel;
        VertexPosition position;
        float scale;
        float rotation;
    };
    std::vector<TreeInstance> treeModels;

    // Main render loop
    ResetCamera();
    GL_OP(glEnable(GL_DEPTH_TEST));
    GL_OP(glClearColor(1.0f, 1.0f, 0.0f, 1.0f));
    while (!glfwWindowShouldClose(pWindow)) {

        GL_OP(glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT));

        // Draw sky
        {
            if (g_regenSkyTexture) {
                spSkyTexture = CreateProcTexture(256, g_skyColor1[g_skyColorIndex], g_skyColor2[g_skyColorIndex]);
                spSkyTexture->SetWrapMode(WrapMode::Repeat);
                sky.SetTexture(0, spSkyTexture.get());
                g_regenSkyTexture = false;
            }

            if (g_animateSky) {
                UseProgram(programTexturing);
                g_texOffset.x += 0.0002f;
                GL_OP(glUniform2f(g_locTexOffset, g_texOffset.x, g_texOffset.y));
            }

            sky.Render();
        }

        GL_OP(glClear(GL_DEPTH_BUFFER_BIT));

        // Draw terrain
        {
            if (g_regenTerrainTexture) {
                spGroundTexture = CreateProcTexture(256, g_groundColor1[g_groundColorIndex], g_groundColor2[g_groundColorIndex]);
                if (spTerrainDynamic) {
                    spTerrainDynamic->SetTexture(0, spGroundTexture.get());
                }

                g_regenTerrainTexture = false;
            }

            if (g_regenTerrainMesh) {
                {
	                TerrainMesh t(g_terrainSize+1, g_ruggedness[g_ruggedIndex]);
                    t.mapToMesh();
                    meshStore.Add("terrain", std::move(t.vertices), std::move(t.faces));
                }

                spTerrainDynamic = std::make_unique<Model>( programFragmentLighting, meshStore.Get("terrain") );
                spTerrainDynamic->SetTexture(0, spGroundTexture.get());
                g_regenTerrainMesh = false;

                treeModels.clear();
                g_regenTrees = true;
            }

            auto& terrainDynamic = *spTerrainDynamic;
            terrainDynamic.SetPolygonMode(g_activeModelPolygonMode);
            terrainDynamic.BeginModelTransformation();
            terrainDynamic.Scale(g_terrainScale);
            terrainDynamic.EndModelTransformation();
            terrainDynamic.PrepareToRender();
            GL_OP(glUniform3f(g_locColor, 1.0f, 0.0f, 0.0f));
            terrainDynamic.Render();
        }

        // Draw trees
        if (g_drawTrees) {

            if (g_regenTreeModels) {
                auto CreateTreeMesh = []()
                {
                    const float presets[][7] =
                    {
                        { 0.18f, 3.0f, 0.4f, 0.1f, 0.4f, 0.5f, 0.2f },
                        { 0.12f, 5.0f, 0.4f, 0.2f, 0.3f, 0.5f, 0.2f },
                    };

                    if (g_treeModelPreset >= sizeof(presets)/sizeof(presets[0])) {
                        g_treeModelPreset = 0;
                    }
                    const auto& params = presets[g_treeModelPreset];

                    ProcTree procTree(params[0], params[1], params[2], params[3], params[4], params[5], params[6]);
                    CalculateVertexNormals(procTree.vertices, procTree.faces);
                    return Mesh::Create("tree", std::move(procTree.vertices), std::move(procTree.faces));
                };
                for (int i = 0; i < g_numTreeModels; ++i) {
                    trees[i] = std::make_shared<Model>(programFragmentLighting, ToShared(CreateTreeMesh()));
                    trees[i]->SetTexture(0, spLeafTexture.get());
                }
                g_regenTreeModels = false;
                treeModels.clear();
                g_regenTrees = true;
            }

            if (g_regenTrees) {
                // We'll use the terrain mesh to note where to place the trees
                auto spTerrainMesh = meshStore.Get("terrain");
                const auto& vertices = spTerrainMesh->Vertices();

                std::random_device rd;
                std::mt19937 gen(rd());
                std::uniform_int_distribution<> dis(0, spTerrainMesh->VertexCount()-1);

                const int numCurrentTrees = static_cast<int>(treeModels.size());
                treeModels.resize(g_numTreeInstances);
                if (numCurrentTrees < g_numTreeInstances) {
                    for (int i = numCurrentTrees; i < g_numTreeInstances; ++i) {
                        const auto vIndex = dis(gen);
                        treeModels[i].spModel = trees[i % g_numTreeModels];
                        treeModels[i].position = vertices[vIndex].position;
                        treeModels[i].scale = 0.08f + 0.005f*(vIndex % 16);
                        treeModels[i].rotation = float(vIndex % 360);
                    }
                }
                g_regenTrees = false;
            }

            for (auto& treeModel : treeModels) {
                auto& tree = *treeModel.spModel;
                const auto& position = treeModel.position;
                tree.SetPolygonMode(g_activeModelPolygonMode);
                tree.BeginModelTransformation();
                tree.Rotate(treeModel.rotation, glm::vec3 { 0.0f, 1.0f, 0.0f });
                tree.Scale(glm::vec3{ treeModel.scale, treeModel.scale, treeModel.scale });
                tree.Translate(position*g_terrainScale);
                tree.EndModelTransformation();

                tree.Render();
            }
        }

        // Draw the Sun
        if (g_drawSun) {
            sun.BeginModelTransformation();
            sun.Translate(glm::vec3{LightPosition});
            sun.EndModelTransformation();

            sun.Render();
        }

        // Swap and input handling
        GLFW_OP(glfwSwapBuffers(pWindow));
        GLFW_OP(glfwPollEvents());
    }
}

int main(int argc, char** argv)
{
    if (!glfwInit()) {
        std::cerr << "error: Failed to initialize GLFW" << std::endl;
        return EXIT_FAILURE;
    }

	srand(static_cast<unsigned int>(time(NULL)));

#define USE_DEBUG_OUTPUT 0
#if USE_DEBUG_OUTPUT
    const int contextVersionMinor = 4;
#else
    const int contextVersionMinor = 0;
#endif

    GLFW_OP(glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4));
    GLFW_OP(glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, contextVersionMinor));
    GLFW_OP(glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE));
#if defined(_DEBUG) && USE_DEBUG_OUTPUT
    GLFW_OP(glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE));
#endif

    GLFWwindow* pWindow = glfwCreateWindow(g_windowWidth, g_windowHeight, "OpenGL Window", NULL, NULL);
    if (!pWindow) {
        std::cerr << "error: Failed to create window" << std::endl;
        return EXIT_FAILURE;
    }

    GLFW_OP(glfwSetWindowSizeCallback(pWindow, OnResize));
    GLFW_OP(glfwSetKeyCallback(pWindow, OnKey));
    GLFW_OP(glfwSetMouseButtonCallback(pWindow, OnMouseButton));
    GLFW_OP(glfwSetScrollCallback(pWindow, OnScroll));
    GLFW_OP(glfwSetCursorPosCallback(pWindow, OnCursor));
    GLFW_OP(glfwMakeContextCurrent(pWindow));

    glewExperimental = GL_TRUE;
    GLenum err = glewInit();
    if (err) {
        return EXIT_FAILURE;
    }
    GL_OP(glGetError());

    // Setup debugging
#if defined(_DEBUG) && USE_DEBUG_OUTPUT
    GL_OP(glDebugMessageCallback(OnDebugInfo, nullptr));
#endif

    try {
        Render(pWindow);
    } catch (std::exception& e) {
        std::cerr << "error: " << e.what() << std::endl;
        return EXIT_FAILURE;
    }

    GLFW_OP(glfwDestroyWindow(pWindow));

    return EXIT_SUCCESS;
}
